# Notes

## Status on Nov 2 2021


* test 3Scale 2.8 community - NS 3scale - ingen uppenbar setup
* prod -
* stdby -
* on-prem 3Scale 2.9 supported - NS 3scale - APIManager

Current latest is 3Scale 2.11

## Needs for jobsearch

3scale can:

* change from query param to http header to provide api-key
* set not generated api-keys.
* work towards internal services (or have a shared secret with the service)
* one domain per backend. This means one product per service.

## Known issues

* Cannot promote via Gitops from staging to prod
* Cannot delete backend, product via Operator
* Cannot keep in sync ui and operator.

Whole feeling of operator is very much early development.

## TODO

* Setup API-portal
