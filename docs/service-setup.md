# Service setup

This document describes how to setup a service to require a API-key to access it.

This doc assumes we have one service (eg jobsearch) per product
(jobsearch.api.jobtechdev.se).

## Create product

Start with creating a product. The following YAML creates a product
`jobsearch`. It will require that a HTTP_header `api_key` is provided
containing the developers application key, retrieved in the developer
portal. All calls is directed to a backend named `jobsearch` that we will define in the next step. All GET calls to
https://jobsearch.test.services.jtech.se/ will be take care of.

```yaml
apiVersion: capabilities.3scale.net/v1beta1
kind: Product
metadata:
  name: jobsearch
spec:
  name: "Jobsearch API"
  systemName: "jobsearch"
  description: "Search among current job ads."
  deployment:
    apicastSelfManaged:
      authentication:
        userkey:
          authUserKey: api_key
          credentials: headers
      stagingPublicBaseURL: "https://jobsearch.test.services.jtech.se"
      productionPublicBaseURL: "https://jobsearch-prod.test.services.jtech.se"
  mappingRules:
    - httpMethod: GET
      pattern: /
      metricMethodRef: hits
      increment: 1
  backendUsages:
    jobsearch:
      path: /
  applicationPlans:
    standard:
      name: "Standard plan"
```

For now can production url be ignored. 3scale operator cannot in a good
gitops friendly way promote configurations to production. We
will need to use separate setups, if we want to use api-keys in develop
and staging.

## Create backend

The backend connect the product with a service. A product can have multiple backends and bind different paths to different backends. We are not using that feature at the moment.

```yaml
apiVersion: capabilities.3scale.net/v1beta1
kind: Backend
metadata:
  name: jobsearch
spec:
  name: "Jobsearch APIs"
  systemName: "jobsearch"
  privateBaseURL: "http://jobsearch.jobsearch-apis-prod"
  description: "Search for job ads."
```

The `systemName` must be the same as the field under `backendUsages`
above. The private url is the kubernetes internal url to the service.

It is possible to have a shared secret between backend and service.
Since this example use a non-public accessible url it is not needed.

## Add route

Finally, we connect a route that 3scale redirect to our jobsearch
service.

```yaml
apiVersion: route.openshift.io/v1
kind: Route
metadata:
  name: jobsearch-route
spec:
  host: jobsearch.test.services.jtech.se
  port:
    targetPort: gateway
  tls:
    insecureEdgeTerminationPolicy: Redirect
    termination: edge
  to:
    kind: Service
    name: apicast-staging
    weight: 100
  wildcardPolicy: None
```

The `host`-field must be the same as `stagingPublicBaseURL` host
in the product yaml.

Observe that the route i binding towards 3scale component APIcast
and not our service jobsearch. This means all traffic will go via
3scale so it can verify API-keys.

## Links

* CRD backend ref -
https://github.com/3scale/3scale-operator/blob/3scale-2.10-stable-prod/doc/backend-reference.md
* CRD product ref - https://github.com/3scale/3scale-operator/blob/3scale-2.10-stable-prod/doc/product-reference.md
