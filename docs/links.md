# Links

Good overview [blog of 3Scale](https://medium.com/@tamber/api-management-security-series-3sacle-part-1-overview-e661d4b703c7)

* [Docs](https://access.redhat.com/documentation/en-us/red_hat_3scale_api_management/2.10)
* [3Scale toolbox](https://access.redhat.com/documentation/en-us/red_hat_3scale_api_management/2.10/html-single/operating_3scale/index#the-threescale-toolbox) - The 3scale toolbox is a Ruby client that enables you to manage 3scale products from the command line.
* [API cast policies](https://access.redhat.com/documentation/en-us/red_hat_3scale_api_management/2.10/html-single/administering_the_api_gateway/index#apicast_policies)
* [Blog om 3Scale API](https://www.itix.fr/blog/deploy-apis-from-your-cicd-pipeline-with-the-3scale-rest-api/#service)


## Test environment

* [Global manager UI](https://master.test.services.jtech.se)
* [3scale-test tenant admin UI](https://3scale-test-admin.test.services.jtech.se)
* [Demo echo service](https://echo-stag.test.services.jtech.se)
