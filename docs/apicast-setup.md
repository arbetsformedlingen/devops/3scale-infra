# APIcast setup

APIcast is a proxy service running on the same cluster as the service you want to serve with an API-key.
APIcast talks to the API manager(APM) to get information about services to serve and get valid API keys.

## Install operator

An APIcast operator needs to be installed on every cluster. The APIcast
operator helps deploy APIcast proxies.

1. Create a project `3scale-apicast-operator`
1. Install the APIcast operator into the project.

## Install APIcast service

### API manager

We will start with creating a Access token in the API manager. This token is used for APIcast to login on the manager.

1. Login to the master (https://master.WILDECARDDOMAIN) (password can be found in system-seed secret in the 3scale project)
1. Select `Account Settings`in the drop down menu in the dashboards header.
1. Select `Personal`and `Tokens`in the left menu.
1. Click `Add Access Token`
1. Give the token a name. Set scopes to `Account Management API` and permission `Read Only`
1. Click `Create Access Token`
1. Copy the token, you will need it later.
1. Click `I have copied the token` (This step is important. You cannot use token before you have clicked.)

### On cluster providing the service

On the cluster providing the service we will create an APIcast proxy.

Firast we create a secret referring to our 3scale master:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: 3scale-master
stringData:
  3scale-master: https://TOKEN@master.WILDCARDDOMAIN/master/api/proxy/configs
```

Replace the strings TOKEN with your token from the API manager created above and WILDCARDDOMAIN with your wildcard domain.
Your file might look like:

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: 3scale-master
stringData:
  3scale-master: https://1234567890absde@master.test.services.jtech.se/master/api/proxy/configs
```

Create an apicast.yaml file:

```yaml
apiVersion: apps.3scale.net/v1alpha1
kind: APIcast
metadata:
  name: my-apicast
spec:
  adminPortalCredentialsRef:
    name: 3scale-master
```

Deploy both files in `3scale-apicast-operator` namespace.
Ensure a deployment shows up and is availiable named `apicast-my-apicas`
