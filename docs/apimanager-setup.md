# API manager setup

API manager (APM) is the master of 3scale managing all administration and account management.
It shall be installed in a cluster within EU.

## Install operator

1. Create a project `3scale`
1. Install the 3scale API management operator into the project.

## Install API manager

Each wildcard domain needs its own API manager. So jobtechdev.se needs one and jtech.se another.

Create a apimanager.yaml for your apimanager:

```yaml
apiVersion: apps.3scale.net/v1alpha1
kind: APIManager
metadata:
  name: apimanager
spec:
  wildcardDomain: test.services.jtech.se
  tenantName: 3scale-test
  resourceRequirementsEnabled: true
  system:
    fileStorage:
      simpleStorageService:
        configurationSecretRef:
          name: af-minio-auth
```
For a production setup external databases (mysql, postgres, 3scale) is probably desired,
as well as an high avaliability(HA) setup. Read more int the
[3scale documentation](https://access.redhat.com/documentation/en-us/red_hat_3scale_api_management/2.10/html/installing_3scale/index

Create a secret containing authentication to the S3 bucket:

```yaml
apiVersion: v1
kind: Secret
type: Opaque
metadata:
    name: af-minio-auth
stringData:
  AWS_ACCESS_KEY_ID: XXXXXXXXXXXXXXXX
  AWS_BUCKET: XXXXXXXX
  AWS_HOSTNAME: XXXXXXX
  AWS_PATH_STYLE: true
  AWS_PROTOCOL: https
  AWS_REGION: af
  AWS_SECRET_ACCESS_KEY: XXXXXXX
```

Apply the two yaml files in the 3scale namespace.

Once up you shall be able to access:

* https://master.test.services.jtech.se
* https://3scale-test-admin.test.services.jtech.se

User and passwords can be found in password can be found in `system-seed` secret in the 3scale project.
